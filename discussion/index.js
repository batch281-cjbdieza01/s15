// Comments

// Comments are parts of code that gets ignored by the langauge
// Comments are ment to described the written code

/*
Two types of comment
	1. Single Line
	2. Multi line
*/

console.log("Hello, World");

// Variables
/*
	- it is used to contain data
*/

// Declairing Variables - tells our devices that a variable name is created and is ready to store data	

/*
	Syntax:
		let/const variableName;

	let - keyword used in declairing a variable
	const - keyword usually used in declairing CONSTANT variables
*/

let myVariable;
console.log(myVariable);

// Initializing variables - the instance when a variable is given it's initial/starting value

/*
	Syntax:
		let/const variableName - value
*/

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice)

const interest = 3.539;

// Reassigning variable values
// Syntax: variableName = newValue;

productName = 'laptop';
console.log(productName)

// interest = 4.489;

// Declares a variable first
let supplier;
// Initialization of values
supplier = "John Smith Tradings";

//Multiple variable declaration

let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand);


// Data types

//String - handles a series of characters that create a word, phrase, or a sentence
let country = 'Philippines';
let province = "Metro Manila";

//Concatinating strings (+ is used)

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting)

// Escape Character (\) in strings is combination with other characters can produce a different effect (\n)

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

// NUmbers
// Integers/whole numbers
let headCount = 32;
console.log(headCount);

//Decimal numbers/fractions
let grade = 98.7;
console.log(grade)

// Exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text/numbers and strings
console.log("John's grade last quarter is " + grade);

// Boolean - used to store values relating to the state of certain things
let isMarried = true;
let inGoodConduct = false;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Array - are special kind of data type that used to store multiple values;

 // Syntax: let/const arrayName = [elementA, elementB, elementC, . . . .]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Objects - another special kind of data type that's used to mimic real world objects/items
/*
	Syntax:
		let/const objectName = {
			propertyA : value,
			propertyB : value
		}
*/

let person = {
	fullName : 'Juan Dela Cruz',
	age : 35,
	isMarried : false,
	contact : ["09127123474", "12321898209"],
	address : {
		houseNumber : '345',
		city : 'Manila'
	}
}

console.log(person);

let myGrades = {
	firstGrading : 98.7,
	secondGrading : 92.1,
	thirdGrading : 90.2,
	fourthGrading : 94.6

}

console.log(myGrades);

// typeof operator - used to determining the type of data or the value of the variables

console.log(typeof myGrades);
console.log(typeof  grades);

// Null - used to intentionally express the absence of a value in a variable declaration/initialization

let spouse = null;

let myNumber = 0;
let myString = '';

// Undefined
let fullName;
console.log(fullName);












